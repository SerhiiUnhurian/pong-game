#include "Ball.h"

Ball::Ball(int x, int y, int size, int speed, SDL_Color color)
        : Rect(x, y, size, size, speed, color) { }

int Ball::getXVel() const {
    return _xVel;
}

int Ball::getYVel() const {
    return _yVel;
}

void Ball::setXVel(int xVel) {
    _xVel = xVel;
}

void Ball::setYVel(int yVel) {
    _yVel = yVel;
}

void Ball::update() {
    _rect.x += (_xVel / 60);
    _rect.y += (_yVel / 60);
}