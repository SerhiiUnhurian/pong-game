#include "Rect.h"

Rect::Rect(int xCenter, int yCenter, int w, int h, int speed, SDL_Color color) {
    _rect.x = _xOrigin = xCenter - w / 2;
    _rect.y = _yOrigin = yCenter - h / 2;
    _rect.w = w;
    _rect.h = h;
    _speed = speed;
    _color = color;
}

int Rect::getX() const {
    return _rect.x;
}
int Rect::getY() const {
    return _rect.y;
}
int Rect::getH() const {
    return _rect.h;
}
int Rect::getW() const {
    return _rect.w;
}

int Rect::getSpeed() const {
    return _speed;
}

void Rect::setX(int x) {
    _rect.x = x;
}

void Rect::setY(int y) {
    _rect.y = y;
}

void Rect::draw(SDL_Renderer *rend) {
    SDL_SetRenderDrawColor(rend, _color.r, _color.g, _color.b, _color.a);
    SDL_RenderFillRect(rend, &_rect);
};

void Rect::reset() {
    _rect.x = _xOrigin;
    _rect.y = _yOrigin;
};