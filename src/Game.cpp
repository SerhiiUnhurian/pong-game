#include "Game.h"

#define PI 3.14

Game::Game(int winW, int winH) : _winHeight(winH), _winWidth(winW){
    std::string errorMsg;

    if (SDL_Init(SDL_INIT_AUDIO) != 0) {
        errorMsg = "error initializing SDL: " + std::string(SDL_GetError());
        throw std::runtime_error(errorMsg);
    }

    if (TTF_Init() != 0) {
        errorMsg = "error initializing SDL_TTF: " + std::string(TTF_GetError());
        throw std::runtime_error(errorMsg);
    }

    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) != 0 )
    {
        errorMsg = "error initializing SDL_mixer: " + std::string(Mix_GetError());
    }

    _win = SDL_CreateWindow("PONG",
                           SDL_WINDOWPOS_CENTERED,
                           SDL_WINDOWPOS_CENTERED,
                           winW, winH, 0);
    if (!_win) {
        errorMsg = "error creating window: " + std::string(SDL_GetError());
        TTF_Quit();
        SDL_Quit();
        throw std::runtime_error(errorMsg);
    }

    _rend = SDL_CreateRenderer(_win, -1, SDL_RENDERER_ACCELERATED);
    if (!_rend) {
        errorMsg = "error creating renderer: " + std::string(SDL_GetError());
        SDL_DestroyWindow(_win);
        TTF_Quit();
        SDL_Quit();
        throw std::runtime_error(errorMsg);
    }

    sounds[0] = Mix_LoadWAV( "../res/pa5a.wav" );
    sounds[1] = Mix_LoadWAV( "../res/pongblipa4.wav" );
    if(!sounds[0] || !sounds[1])
    {
        errorMsg = "error loading sound: " + std::string(Mix_GetError());
    }

    _font = TTF_OpenFont("../res/Peepo.ttf", _winHeight / 10);
    if (!_font) {
        errorMsg = "error loading font: " + std::string(TTF_GetError());
        SDL_DestroyWindow(_win);
        SDL_DestroyRenderer(_rend);
        TTF_Quit();
        SDL_Quit();
        throw std::runtime_error(errorMsg);
    }

    SDL_Color color;
    color.r = color.g = color. b = 255;
    _lPaddle = new Paddle(35, _winHeight / 2, 12, _winHeight / 4, 540, color);
    _rPaddle = new Paddle(_winWidth - 35, _winHeight / 2, 12, _winHeight / 4, 540, color);
    _ball = new Ball(_winWidth / 2, _winHeight / 2, 16, 960, color);
}

Game::~Game() {
    delete _lPaddle;
    delete _rPaddle;
    delete _ball;

    for (Mix_Chunk *s : sounds) Mix_FreeChunk(s);
    SDL_DestroyWindow(_win);
    SDL_DestroyRenderer(_rend);
    TTF_CloseFont(_font);
    Mix_Quit();
    TTF_Quit();
    SDL_Quit();
}

void Game::run() {
    srand(time(nullptr));
    _leftTurn = rand() % 2 != 0;

    _keystates = SDL_GetKeyboardState(nullptr);
    _running = true;

    while (_running) {
        draw();
        input();
        update();
    }
}

void Game::update() {
    int lPaddleY = _lPaddle->getY();
    int lPaddleH = _lPaddle->getH();
    int rPaddleY = _rPaddle->getY();
    int rPaddleH = _rPaddle->getH();
    int ballX = _ball->getX();
    int ballY = _ball->getY();
    int ballH = _ball->getH();

    if(SDL_HasIntersection((SDL_Rect*)_ball, (SDL_Rect*)_lPaddle)) {
        Mix_PlayChannel( -1, sounds[0], 0 );
        double rel = (lPaddleY + (lPaddleH / 2.0)) - (ballY + (ballH / 2.0));
        double norm = rel / (lPaddleH / 2.0);
        double bounce = norm * (5 * PI / 12);
        _ball->setXVel(_ball->getSpeed() * cos(bounce));
        _ball->setYVel(_ball->getSpeed() * -sin(bounce));
    }
    if(SDL_HasIntersection((SDL_Rect*)_ball, (SDL_Rect*)_rPaddle)) {
        Mix_PlayChannel( -1, sounds[0], 0 );
        double rel = (rPaddleY + (rPaddleH / 2.0)) - (ballY + (ballH / 2.0));
        double norm = rel / (rPaddleH / 2.0);
        double bounce = norm * (5 * PI / 12);
        _ball->setXVel(-_ball->getSpeed() * cos(bounce));
        _ball->setYVel(_ball->getSpeed() * -sin(bounce));
    }

    if (ballX <= 0) {
        Mix_PlayChannel( -1, sounds[1], 0 );
        scoreUp(_rPaddle);
    } else if (ballX >= _winWidth - ballH) {
        Mix_PlayChannel( -1, sounds[1], 0 );
        scoreUp(_lPaddle);
    }
    if (ballY <= 0) {
        _ball->setYVel(-_ball->getYVel());
    } else if (ballY + ballH >= _winHeight) {
        _ball->setYVel(-_ball->getYVel());
    }


    if (lPaddleY < 0) _lPaddle->setY(0);
    if (lPaddleY + lPaddleH > _winHeight) _lPaddle->setY(_winHeight - lPaddleH);
    if (rPaddleY < 0) _rPaddle->setY(0);
    if (rPaddleY + rPaddleH > _winHeight) _rPaddle->setY(_winHeight - rPaddleH);

    _ball->update();
    // Update Player's paddle position
    if (_keystates[SDL_SCANCODE_UP] && !_keystates[SDL_SCANCODE_DOWN]) _lPaddle->moveDown();
    if (_keystates[SDL_SCANCODE_DOWN] && !_keystates[SDL_SCANCODE_UP]) _lPaddle->moveUp();
    // Update Computer's paddle position
    if(ballY > (rPaddleY + (rPaddleH / 2))) _rPaddle->moveUp();
    if(ballY + ballH < (rPaddleY + (rPaddleH / 2))) _rPaddle->moveDown();
}


void Game::input() {
    while (SDL_PollEvent(&_event))
    {
        if (_event.type == SDL_QUIT) _running = false;
    }
    if (_keystates[SDL_SCANCODE_SPACE]) {
        if (_leftTurn) {
            _ball->setXVel(_ball->getSpeed() / 2);
        } else {
            _ball->setXVel(-_ball->getSpeed() / 2);
        }
    }
}

void Game::draw() {
    SDL_SetRenderDrawColor(_rend, 0, 0, 0, 255);
    SDL_RenderClear(_rend);
    _lPaddle->draw(_rend);
    _rPaddle->draw(_rend);
    _ball->draw(_rend);
    drawScore();
    SDL_RenderDrawLine(_rend, _winWidth / 2, 0, _winWidth / 2, _winHeight);
    SDL_RenderPresent(_rend);
    SDL_Delay(1000/60);
}

void Game::drawScore() {
    drawText(std::to_string(_lScore),
             _winWidth / 4,
             _winHeight * 0.2,
             {255,255,255});
    drawText(std::to_string(_rScore),
             _winWidth - _winWidth / 4,
             _winHeight * 0.2,
             {255,255,255});
}

void Game::drawText(const std::string &text, int xCenter, int yCenter , SDL_Color color) {
    SDL_Surface *surface;
    SDL_Texture *texture;
    SDL_Rect board;
    const char *t = text.c_str();
    surface = TTF_RenderText_Solid(_font, t, {color.r, color.g, color.b});
    texture = SDL_CreateTextureFromSurface(_rend, surface);
    board.w = surface->w;
    board.h = surface->h;
    board.x = xCenter - board.w / 2;
    board.y = yCenter - board.h;
    SDL_FreeSurface(surface);
    SDL_RenderCopy(_rend, texture, nullptr, &board);
    SDL_DestroyTexture(texture);
}

void Game::reset() {
    _lPaddle->reset();
    _rPaddle->reset();
    _ball->reset();
    _ball->setXVel(0);
    _ball->setYVel(0);
}

void Game::scoreUp(Paddle *paddle) {
    if (paddle == _lPaddle) {
        _lScore++;
        _leftTurn = false;
    }
    if (paddle == _rPaddle) {
        _rScore++;
        _leftTurn = true;
    }
    if (_lScore == 3 || _rScore == 3) won();

    reset();
}

void Game::won() {
    std::string msg;
    (_lScore == 3) ? msg = "You win!" : msg = "You lose!";

    SDL_SetRenderDrawColor(_rend, 0, 0, 0, 255);
    SDL_RenderClear(_rend);
    drawText(msg, _winWidth / 2, _winHeight / 2, {255,255,255});
    SDL_RenderPresent(_rend);
    SDL_Delay(3000);
    _lScore = _rScore = 0;
}