#ifndef PONG_GAME_H
#define PONG_GAME_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <iostream>
#include <vector>
#include <array>

#include "Ball.h"
#include "Paddle.h"

class Game {
private:
    SDL_Renderer *_rend;
    SDL_Window *_win;
    TTF_Font *_font;
    int _winWidth, _winHeight;
    Paddle *_lPaddle, *_rPaddle;
    Ball *_ball;
    int _lScore {}, _rScore {};
    bool _leftTurn {false};
    bool _running {false};
    SDL_Event _event {};
    const Uint8 *_keystates {nullptr};
    std::array<Mix_Chunk*, 2> sounds {};

    void drawText(const std::string& text, int x, int y, SDL_Color color);
    void drawScore();
    void scoreUp(Paddle *paddle);
    void won();
    void input();
    void update();
    void draw();
    void reset();

public:
    Game(int winW, int winH);
    ~Game();

    void run();
};

#endif //PONG_GAME_H
