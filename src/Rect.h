#ifndef PONG_RECT_H
#define PONG_RECT_H

#include <SDL2/SDL.h>

class Rect {
protected:
    SDL_Rect _rect {};
    int _xOrigin {}, _yOrigin{};
    int _speed;
    SDL_Color _color {};

public:
    Rect(int xCenter, int yCenter, int w, int h, int speed, SDL_Color color);

    int getX() const;
    int getY() const;
    int getH() const;
    int getW() const;
    int getSpeed() const;

    void setX(int x);
    void setY(int y);

    void draw(SDL_Renderer *rend);
    void reset();
};

#endif //PONG_RECT_H
