#include "Paddle.h"

Paddle::Paddle(int x, int y, int w, int h, int speed, SDL_Color color)
            : Rect(x, y, w, h, speed, color) { };

void Paddle::moveUp() {
    _rect.y += _speed / 60;
}
void Paddle::moveDown() {
    _rect.y -= _speed / 60;
}