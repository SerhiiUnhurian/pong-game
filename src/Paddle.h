#ifndef PONG_PADDLE_H
#define PONG_PADDLE_H

#include "Rect.h"

class Paddle : public Rect {
public:
    Paddle(int x, int y, int w, int h, int speed, SDL_Color color);

    void moveUp();
    void moveDown();
};

#endif //PONG_PADDLE_H
