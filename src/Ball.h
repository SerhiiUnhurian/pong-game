#ifndef PONG_BALL_H
#define PONG_BALL_H

#include "Rect.h"

class Ball : public Rect {
private:
    int _xVel {}, _yVel {};

public:
    Ball(int x, int y, int size, int speed, SDL_Color color);

    int getXVel() const;
    int getYVel() const;

    void setXVel(int xVel);
    void setYVel(int yVel);

    void update();
};

#endif //PONG_BALL_H
