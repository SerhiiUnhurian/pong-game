#include <Game.h>

int main() {
    try {
        Game pong(720, 720);
        pong.run();
    }
    catch (std::runtime_error &e) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}